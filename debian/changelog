osmo-hlr (1.8.0+dfsg1-2) unstable; urgency=medium

  * upload to unstable

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 02 Sep 2024 20:33:25 +0200

osmo-hlr (1.8.0+dfsg1-1) experimental; urgency=medium

  [ Nathan Doris ]
  * debian/control
   - added help2man dependency
   - libosmocore-dev 1.7.0 -> 1.10.0
   - libosmo-abis 1.3.0 -> 1.6.0
   - replaced pkg-config -> pkgconf
   - new standards 4.7.0 no changes
   - Rules-Requires-Root: no
  * debian/libosmo-mslookup*
   - bumped version to 1
   - created new symbols file
  * debian/patches
   - new patch autotools-pkg-config-macro-not-cross-compilation-safe.patch
   - new patch no-manual-page.patch
   - new patch spelling.patch
  * debian/help2man : new folder
   - 4 new *.h2m files for manual pages
  * debian/upstream
   - new folder and new file "metadata"
  * debian/copyright
   - refreshed copyright

  [ Thorsten Alteholz ]
  * New upstream release

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 14 Aug 2024 16:33:25 -0600

osmo-hlr (1.5.0+dfsg1-4) unstable; urgency=medium

  * due to dependencies (libosmo-netif) package no longer builds on
    32bit architectures, so we don't need this t64 versions any longer
  * debian/control: bump standard to 4.7.0 (no changes)

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 20 Apr 2024 12:42:29 +0200

osmo-hlr (1.5.0+dfsg1-3) unstable; urgency=medium

  * debian/rules: configure with --with-systemdsystemunitdir=no
                  thanks to Dan Bungert and Gianfranco Costamagna
                  for the suggestion (Closes: #1021763)

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 15 Oct 2022 12:42:29 +0200

osmo-hlr (1.5.0+dfsg1-2) unstable; urgency=medium

  * upload to unstable

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 13 Oct 2022 20:53:42 +0200

osmo-hlr (1.5.0+dfsg1-1) experimental; urgency=medium

  * New upstream release.
  * debian/control: use at least libosmocore 1.7.0
  * debian/control: use at least libosmo-abis 1.3.0
  * debian/control: use at least libosmo-netif 1.2.0

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 03 Oct 2022 17:48:12 +0200

osmo-hlr (1.4.0+dfsg1-3) unstable; urgency=medium

  * upload to unstable

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 16 Mar 2022 20:53:42 +0100

osmo-hlr (1.4.0+dfsg1-2) experimental; urgency=medium

  * debian/control: use correct Section: and Architecture for
                    all packages

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 10 Mar 2022 02:53:42 +0100

osmo-hlr (1.4.0+dfsg1-1) experimental; urgency=medium

  * New upstream release
  * debian/control: bump standard to 4.6.0 (no changes)
  * debian/control: use at least libosmocore 1.5.0
  * debian/control: use at least libosmo-abis 1.1.0
  * debian/control: use at least libosmo-netif 1.1.0
  * debian/control: use dh13

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 09 Mar 2022 03:53:42 +0000

osmo-hlr (1.2.1+dfsg1-1) unstable; urgency=medium

  * New upstream release
  * add 0001-debian-rules-Remove-example-Makefile.patch (Closes: #977561)
    Thanks to Vagrant Cascadian for the patch and making the package
    build reproducible.

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 06 Jan 2021 19:21:23 +0000

osmo-hlr (1.2.0+dfsg1-2) unstable; urgency=medium

  * upload to unstable
  * mark test as superficial (Closes: #971487)

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 10 Dec 2020 18:15:42 +0100

osmo-hlr (1.2.0+dfsg1-1) experimental; urgency=medium

  * New upstream release
  * debian/control: bump standard to 4.5.0 (no changes)
  * debian/control: use dh12
  * debian/control: depend on libosmocore >=1.4.0
  * debian/control: depend on libosmo-abis >= 1.0.1
  * debian/control: depend on libosmo-netif >= 1.0.0
  * debian/control: take care of soname change
  * add Build-Depends-Package: to symbols files
  * remove patches that are applied upstream

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 16 Aug 2020 23:15:42 +0000

osmo-hlr (0.2.1-5) unstable; urgency=medium

  * debian/control:
    - Remove python-minimal dependency (Closes: #937221)
  * 0004-Remove-auc-test-since-it-depends-on-Python2.patch:
    - Remove the auc test which depends on Python2.
    - This also resolves the test suite on s390x (Closes: #918674)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 21 Mar 2020 21:14:05 +0100

osmo-hlr (0.2.1-4) unstable; urgency=low

  * Re-enable tests on all architectures
  * Patch for test suite on some archs:
    - 0003-Fix-test-for-return-codes-on-mipsel-and-alpha-archs.patch

 -- Ruben Undheim <rubund@debian.org>  Fri, 16 Nov 2018 08:38:18 +0100

osmo-hlr (0.2.1-3) unstable; urgency=medium

  * debian/control:
    - Add my name to uploaders
  * debian/rules:
    - Disable tests on the architectures mips64el, mipsel and alpha.

 -- Ruben Undheim <rubund@debian.org>  Fri, 09 Nov 2018 22:19:28 +0100

osmo-hlr (0.2.1-2) unstable; urgency=medium

  * Team upload
  * Upload to unstable

 -- Ruben Undheim <ruben.undheim@gmail.com>  Tue, 06 Nov 2018 07:54:48 +0100

osmo-hlr (0.2.1-2~exp1) experimental; urgency=medium

  * Team upload
  * debian/gbp.conf: to enforce pristine-tar
  * Added default configuration file to /etc/osmocom/osmo-hlr.cfg
  * debian/control: New standards version 4.2.1 - no changes
  * debian/osmo-hlr.dirs:
    - Create dir expected to be present when starting osmo-hlr at first
  * debian/rules:
    - Do not start systemd service by default
    - Properly clean up in override_dh_clean
  * debian/tests:
    - Add simple autopkgtest checking that it is possible to run "osmo-hlr -h"

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 23 Sep 2018 15:43:13 +0200

osmo-hlr (0.2.1-1) experimental; urgency=medium

  * New upstream release
  * debian/control: adjust minimal version of dependencies

 -- Thorsten Alteholz <debian@alteholz.de>  Fri, 18 May 2018 11:06:52 +0200

osmo-hlr (0.1.0-3) unstable; urgency=medium

  * debian/rules: deactivate tests for BE for now

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 19 Apr 2018 19:07:49 +0200

osmo-hlr (0.1.0-2) unstable; urgency=medium

  * move to unstable
  * debian/control: add salsa URLs
  * debian/control: use dh11
  * debian/control: bump standard to 4.1.4 (no changes)
  * debian/control: move package to debian-mobcom

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 16 Apr 2018 19:05:39 +0200

osmo-hlr (0.1.0-1) experimental; urgency=medium

  * Initial release

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 13 Dec 2017 19:05:39 +0100
